# particless Dynare module

This package, used in [Dynare](http://www.dynare.org), provides
nonlinear filtering algorithms. The documentation is available in the
[doc](https://git.dynare.org/Dynare/particles/tree/master/doc)
subfolder.
